title: Episode 15: At Libre Planet with Sean O'Brien
date: 2019-04-05 14:00:00
tags: episode, privacy
summary: At Libre Planet with Sean O'Brien
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-015/librelounge-ep-015.mp3" length:"15911785" duration:"00:37:33"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-015/librelounge-ep-015.ogg" length:"21820653" duration:"00:37:33"
---

We're still at Libre Planet 2019 and we're talking to Sean O'Brien from the Yale Privacy Lab and Purism about personal security, phones, and how stores use sounds you can't hear to spy on you.

Links:

- [Yale Privacy Lab (yale.edu)](https://privacylab.yale.edu/)
- [The LibreM Phone (puri.sm)](https://puri.sm/products/librem-5/)
- [Exodus Privacy (exodus-privacy.eu.org)](https://exodus-privacy.eu.org/en/)
- [F-Droid (f-droid.org)](https://f-droid.org)
- [LineageOS (lineageos.org)](https://lineageos.org/)
- [Uber Tracks Users After They've Ended Their Ride (theguardian.com)](https://www.theguardian.com/technology/2017/aug/29/uber-u-turn-tracking-users-after-trip-ended-app-user-privacy-new-ceo
)
- [Uber Stiffling Lyft (theverge.com)](https://www.theverge.com/2014/8/26/6067663/this-is-ubers-playbook-for-sabotaging-lyft)
- [Uber's Secret Program to Cripple Lyft (theguardian.com)](https://www.theguardian.com/technology/2017/apr/13/uber-allegedly-used-secret-program-to-cripple-rival-lyft)
- [Uber Stiffles Competition According to New Lawsuit (arstechnica.com)](https://arstechnica.com/tech-policy/2018/12/uber-was-hell-bent-on-stifling-competition-new-lawsuit-alleges/
)
- [Uber uses software to evade regulators (reuters)](https://www.reuters.com/article/us-uber-portland/portland-probe-finds-uber-used-software-to-evade-16-government-officials-idUSKCN1BQ08Z
)
- [Facebook Consider End-to-End Encryption (theverge.com)](https://www.theverge.com/2019/1/25/18197222/facebook-messenger-instagram-end-to-end-encryption-feature-zuckerberg)
- [Facebook Looking to a WeChat Strategy (theverge.com)](https://www.theverge.com/2019/3/8/18256226/facebook-wechat-messaging-zuckerberg-strategy)
- [WeChat in China (theverge.com)](https://www.theverge.com/2018/2/1/16721230/wechat-china-app-mini-programs-messaging-electronic-id-system)
- [China's Social Credit System (businessinsider.com)](https://www.businessinsider.com/china-social-credit-system-punishments-and-rewards-explained-2018-4)
- [Wuffie (wikipedia)](https://en.wikipedia.org/wiki/Whuffie)
- [Nokia N900 (wikipedia)](https://en.wikipedia.org/wiki/Nokia_N900)
- [LibHandy (gnome.org)](https://gitlab.gnome.org/Community/Purism/libhandy)
- [List of Open Source Phones (wikipedia)](https://en.wikipedia.org/wiki/List_of_open-source_mobile_phones)
- [ArduinoPhone (instructables)](https://www.instructables.com/id/ArduinoPhone/)
