title: Episode 3: Hacker Culture, Past, Belonging and Inclusion
date: 2018-12-08 02:30:00
tags: episode, hacker culture
summary: Hacker Culture, Past, Belonging and Inclusion
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-003/librelounge-ep-003.mp3"
---

In this episode of Libre Lounge, Serge and Chris go back to the roots of hacker culture starting in the 1950s and 1960s and connecting that with the hacker culture of today, its challenges and how it needs to evolve moving forward.

Show links:

- [*Hackers: Heroes of the Computer Revolution* by Steven Levy (stevenlevy.com)](https://www.stevenlevy.com/index.php/books/hackers)
- [Free as in Freedom (sagitter.fedorapeople.org)](https://sagitter.fedorapeople.org/faif-2.0.pdf)
- [Programming is Forgetting: Toward a New Hacker Ethic (opentranscripts)](http://opentranscripts.org/transcript/programming-forgetting-new-hacker-ethic/)
- [The Problem with the Hacker Mystique (youtube)](https://www.youtube.com/watch?v=KlZ22xmFNOU)
- [Eric Raymond's Jargon File (catb.org)](http://catb.org/jargon/html/)
- [The Original Jargon File (dourish.com)](http://www.dourish.com/goodies/jargon.html)
- [Hackerspaces (hackerspaces.org)](https://hackerspaces.org)
- [Maker Movement (wikipedia)](https://en.wikipedia.org/wiki/Maker_culture)
- [MAKE Magazine (makezine.com)](https://makezine.com/)
- [Life hack (wikipedia)](https://en.wikipedia.org/wiki/Life_hack)
- **CW** [Chris's article on depression (dustyweb)](https://dustycloud.org/blog/on-hackers-and-depression/)
- **CW** [Mitch Altman on Geek and Depression (bluehackers.org)](https://bluehackers.org/2013/01/17/mitch-altman-geeks-and-depression-panel-at-28c3)
- **CW** [Jason Scott on Geeks and Suicide (textfiles.com)](http://ascii.textfiles.com/archives/3898)
- [The Microsoft Ad (ispot.tv)](https://www.ispot.tv/ad/duBN/microsoft-surface-pro-6-adam-wilson-building-robots-and-a-business)
- [Poochie (simpsons.wikia.com)](http://simpsons.wikia.com/wiki/Poochie)
- [Wargames (wikipedia)](https://en.wikipedia.org/wiki/WarGames)
- [Hackers (wikipedia)](https://en.wikipedia.org/wiki/Hackers_(film))
- [For the Love of Hacking (forbes)](https://www.forbes.com/forbes/1998/0810/6203094a.html#2f0471ba79bc)
- [RepRap (reprap.org)](https://reprap.org)
- [Makerbot goes Proprietary (cnet)](https://www.cnet.com/news/pulling-back-from-open-source-hardware-makerbot-angers-some-adherents/)
- [The Illegal Tattoo (treachery.net)](http://www.treachery.net/~jdyson/crypto/tattoo.html)
- [A Portrait of J. Random Hacker (catb.org)](http://www.catb.org/jargon/html/appendixb.html)

