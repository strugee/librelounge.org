title: Episode 17: ActivityPub Part 2
date: 2019-04-19 00:00:00
tags: episode, activitypub, json-ld, activitystreams
summary: ActivityPub Part 2
enclosure: title:mp3 url:"https://archive.org/download/librelounge-ep-017/librelounge-ep-017.mp3" length:"18827158" duration:"00:49:56"
enclosure: title:ogg url:"https://archive.org/download/librelounge-ep-017/librelounge-ep-017.ogg" length:"28888319" duration:"00:49:56"
---
In our ongoing series about ActivityPub, Chris and Serge explore the world of JSON-LD and the ActivityStreams vocabulary.

Listen to Part 1 **[Here](https://librelounge.org/episodes/episode-12-activitypub-part-1.html)**

Links:

- [The ActivityPub Specification (w3c)](https://www.w3.org/TR/activitypub/)
- [ActivityStreams (w3c)](https://www.w3.org/TR/activitystreams-core/)
- [JSON-LD (json-ld.org)](https://json-ld.org/)
- [JSON-LD Playground (json-ld.org)](https://json-ld.org/playground/)
- [Cyc (wikipedia)](https://en.wikipedia.org/wiki/Cyc)
- [RDF (w3c)](https://www.w3.org/RDF/)
